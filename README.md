# Godot Tools Test
This repo contains a number of utilities and experiments for the game engine [Godot](https://godotengine.org/)

In order to run the project, make sure you have Godot version 4.1 or later installed https://godotengine.org/download/

## Dialog
This utility can generate styled dialog using a custom syntax

![Gif embed of dialog node](./_README_ASSETS/DIALOG_EXAMPLE.gif)

Example usage can be found in [Main.tscn](./Main.tscn)