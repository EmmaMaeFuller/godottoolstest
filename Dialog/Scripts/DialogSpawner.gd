class_name DialogSpawner extends Node2D

signal dialog_ended

## Time delay between each individual letter being drawn to screen
@export_range(0.001, 1.0) var text_speed := 0.1
## Time delay between drawing the end of one line and the beginning of another
@export_range(0.001, 1.0) var line_speed := 0.5
@export var font_size := 36
@export var font_face: FontFile

@export_category("Dialog")
@export_multiline var Text: String
@export var EffectsMap: Dictionary

var clock: DialogClock
var theme: Theme

var text_segments: Array[DialogSegment]
var current_segment: DialogSegment
var offset := Vector2(0.0, 0.0)
var y_height := 0.0


func _ready():
	$BackgroundSprite.visible = false
	clock = DialogClock.new()
	clock.tick.connect(_do_text_draw)
	clock.timer_speed = text_speed
	add_child(clock)

	theme = Theme.new()
	theme.default_font = font_face
	theme.default_font_size = font_size


func _setup():
	$BackgroundSprite.visible = true
	text_segments = DialogParser.parse_text_input(Text, EffectsMap)
	current_segment = text_segments.pop_front()
	visible = true
	clock.start()


func _teardown():
	clock.stop()
	y_height = 0.0
	offset = Vector2(0.0, 0.0)
	for child in $TextContainer.get_children():
		child.queue_free()


func kickoff_dialog(dialog: String):
	Text = dialog
	_teardown()
	_setup()


func _unhandled_input(event):
	if event.is_action_pressed("Advance Text"):
		_teardown()
		_setup()


func _do_text_draw():
	if current_segment == null:
		# We've reached the end of the dialog
		dialog_ended.emit()
		clock.stop()
		return

	var next_letter = current_segment.get_next_letter()

	if next_letter == null:
		# We've reached the end of the dialog segment
		current_segment = text_segments.pop_front()
	elif next_letter == "\n":
		offset = Vector2(0, offset.y + y_height)
		clock.stop_for.call_deferred(line_speed)
	else:
		var letter := current_segment.create_instance(offset, next_letter, theme)
		_increase_letter_offset.call_deferred(letter)
		$TextContainer.add_child(letter)

		if next_letter == " ":
			_do_text_draw.call_deferred()


func _increase_letter_offset(label: Label):
	offset += Vector2(label.get_rect().size.x, 0.0)
	y_height = label.get_rect().size.y
