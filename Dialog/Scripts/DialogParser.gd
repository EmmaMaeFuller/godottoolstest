class_name DialogParser extends Node

static var EFFECT_TOKEN := RegEx.create_from_string("^(?<effect>\\w+){{(?<text>.*?)}}")
static var SEGMENT_TOKEN := RegEx.create_from_string("^(?<text>\\S+)")
static var WHITESPACE_TOKEN := RegEx.create_from_string("^(?<text>\\s+)")


static func parse_text_input(text: String, effect_map: Dictionary) -> Array[DialogSegment]:
	var segments: Array[DialogSegment] = []
	var text_buffer := text.strip_edges()

	while text_buffer.length() > 0:
		var token: RegExMatch
		var segment := DialogSegment.new()

		token = EFFECT_TOKEN.search(text_buffer)
		if not token:
			token = SEGMENT_TOKEN.search(text_buffer)
		if not token:
			token = WHITESPACE_TOKEN.search(text_buffer)
		assert(token != null, "UNPARSABLE STRING %s" % text_buffer)

		text_buffer = text_buffer.trim_prefix(token.get_string())
		segment.text = token.get_string("text")

		if effect_map.has(token.get_string("effect")):
			segment.effect = effect_map.get(token.get_string("effect"))

		segments.push_back(segment)

	return segments
