class_name DialogClock extends Node

var timer: Timer
var timer_speed: float
signal tick


func _ready():
	timer = Timer.new()
	timer.one_shot = false
	timer.timeout.connect(_do_tick)

	add_child(timer)
	timer.start(timer_speed)


func _do_tick():
	tick.emit()


func start():
	timer.start(timer_speed)


func stop():
	timer.stop()


func stop_for(amount: float):
	timer.paused = true
	await get_tree().create_timer(amount).timeout
	timer.paused = false
