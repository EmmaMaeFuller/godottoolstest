class_name DialogSegment extends Node

var text: String
var effect = Label
var current_index := 0


func get_next_letter():
	if current_index >= text.length():
		return null

	var returned_letter := text[current_index]
	current_index = current_index + 1
	return returned_letter


func create_instance(position_: Vector2, text_: String, theme_: Theme) -> Label:
	var instance = effect.new()
	instance.position = position_
	instance.text = text_
	instance.theme = theme_

	return instance
