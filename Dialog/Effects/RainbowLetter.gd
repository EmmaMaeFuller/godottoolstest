class_name RainbowLetter extends Label

var progress := 0.0


func _init():
	change_values()


func _process(delta):
	progress += delta
	change_values()


func change_values():
	add_theme_color_override("font_color", Color.from_hsv(progress, 1.0, 1.0))
