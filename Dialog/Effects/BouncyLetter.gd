class_name BouncyLetter extends Label

var offset := 2.0
var time_step := 0.2


func _init():
	shake_x.call_deferred()
	shake_y.call_deferred()


func shake_x():
	position.x -= offset / 2
	while true:
		position.x += offset
		await create_delay()
		position.x -= offset
		await create_delay()


func shake_y():
	position.y -= offset / 2
	while true:
		position.y += offset
		await create_delay()
		position.y -= offset
		await create_delay()


func create_delay() -> Signal:
	return get_tree().create_timer(randf_range(0, time_step)).timeout
