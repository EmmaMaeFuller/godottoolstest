extends Node2D

var running := false

@onready var dBox := $DialogBox as DialogSpawner


func do_some_dialog():
	if running:
		return
	running = true

	var dialog_entries = [
		"""
			Hey, this is the demo of my
			really sweet cool dialog system
		""",
		"""
			What makes it cool you say?
			Does RAINBOW{{THIS}} answer your question?
		""",
		"""
			BOUNCY{{N..no really..does it?}}
			BOUNCY{{I'm honestly not sure...}}
		""",
		"""
			Well I'm having fun with it at least
			And isn't that what matters? ...
		""",
		"""
			RED{{THE END}}
		""",
	]

	for entry in dialog_entries:
		dBox.kickoff_dialog(entry)
		await dBox.dialog_ended
		await _wait_for(2.0)

	dBox.visible = false
	running = false


func _wait_for(amount: float):
	return get_tree().create_timer(amount).timeout
